// ==UserScript==
// @name         New Userscript
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://gandalf.epitech.eu/mod/workshop/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    function noteWith(note, message) {
     document.getElementsByClassName('rubric-grid')[0].childNodes[1].childNodes.forEach((elem => {
        if(elem.tagName=='TR' && elem.childElementCount !== 2){
            elem.children[note+1].firstElementChild.children[1].firstElementChild.firstElementChild.firstElementChild.firstElementChild.checked = true
        }
     }))
     document.getElementById('id_feedbackauthor_editoreditable').firstElementChild.textContent = message
     resolve()
    }
    function noteWithRand(message) {
     document.getElementsByClassName('rubric-grid')[0].childNodes[1].childNodes.forEach((elem => {
        if(elem.tagName=='TR' && elem.childElementCount !== 2){
            var note = Math.floor(Math.random()*5 + 1)
            elem.children[note].firstElementChild.children[1].firstElementChild.firstElementChild.firstElementChild.firstElementChild.checked = true
        }
     }))
     document.getElementById('id_feedbackauthor_editoreditable').firstElementChild.textContent = message
     resolve()
    }

    function resolve() {
        if(document.getElementById('id_saveandshownext')) document.getElementById('id_saveandshownext').click()
        else document.getElementById('id_saveandclose').click()
    }

    function submit(){
        var radio0 = document.getElementById('radio0')
        var radio1 = document.getElementById('radio1')
        var radio2 = document.getElementById('radio2')
        var radio3 = document.getElementById('radio3')
        var radio4 = document.getElementById('radio4')
        var radioRand = document.getElementById('radioRand')
        var fname = document.getElementById('fname')

        if(radio0.checked) noteWith(0, fname.value)
        if(radio1.checked) noteWith(1, fname.value)
        if(radio2.checked) noteWith(2, fname.value)
        if(radio3.checked) noteWith(3, fname.value)
        if(radio4.checked) noteWith(4, fname.value)
        if(radioRand.checked) noteWithRand(fname.value)
    }
    var div = document.createElement('div')
    div.innerHTML = '<input type="radio" id="radio0" name="note" value="1"><label for="radio0">N/A</label><br><input type="radio" id="radio1" name="note" value="2"><label for="radio1">1</label><br><input type="radio" id="radio2" name="note" value="3"><label for="radio2">2</label><br><input type="radio" id="radio3" name="note" value="4"><label for="radio3">3</label><br><input type="radio" id="radio4" name="note" value="5"><label for="radio4">4</label><br><input type="radio" id="radioRand" name="note" value="rand"><label for="radioRand">Random</label><br><label for="fname">Feedback for the author:</label><br><input type="text" id="fname" name="fname"><button id="showNextBtn">saveAndShowNext</button>'
    document.getElementById('region-main').prepend(div)
    document.getElementById('showNextBtn').addEventListener("click", submit)
})();